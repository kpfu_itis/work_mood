from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import RetrieveModelMixin, ListModelMixin, UpdateModelMixin, DestroyModelMixin, \
    CreateModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from question.models import Quiz, UserQuestion
from question.serializers import QuizSerializer


class QuizView(
    CreateModelMixin,
    RetrieveModelMixin,
    ListModelMixin,
    UpdateModelMixin,
    DestroyModelMixin,
    GenericViewSet
):

    queryset = Quiz.objects.all()
    serializer_class = QuizSerializer

    def get_queryset(self):
        queryset = super(QuizView, self).get_queryset()
        return queryset.filter(user=self.request.user).order_by('-created_at')

    def list(self, request, *args, **kwargs):
        serializer = self.get_serializer(self.get_queryset(), many=True)
        return JsonResponse(data=serializer.data, safe=False)

    def create(self, request, *args, **kwargs):
        quiz = Quiz()
        quiz.user = request.user
        quiz.save()
        for i in range(3):
            user_question = UserQuestion()
            user_question.user = request.user
            user_question.question_id = i + 1
            user_question.answer = request.data.get(f'answer_{i + 1}')
            user_question.save()
            quiz.questions.add(user_question)

        return Response(200)


class TrackerView(GenericAPIView):

    queryset = Quiz.objects.all()
    serializer_class = QuizSerializer

    def get_queryset(self):
        queryset = super(TrackerView, self).get_queryset()
        return queryset.filter(user=self.request.user).order_by('-created_at')

    def get(self, request, **kwargs):
        serializer = self.get_serializer(self.get_queryset(), many=True)
        return render(
            request,
            'question/tracker.html',
            {
                'user': request.user,
                'questions': serializer.data
            }
        )
