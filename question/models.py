from django.db import models


class Question(models.Model):

    title = models.CharField(
        verbose_name='Заголовок вопроса',
        null=True, blank=True,
        max_length=30,
    )

    text = models.TextField(
        verbose_name='Текст вопроса',
        null=True, blank=True,
    )

    is_options = models.BooleanField(
        verbose_name='Есть варианты ответа?',
        default=False,
    )

    is_multiple = models.BooleanField(
        verbose_name='Множественный выбор?',
        default=False
    )

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'


class Answer(models.Model):

    question = models.ForeignKey(
        to=Question,
        related_name='variants',
        verbose_name='Вопрос',
        on_delete=models.CASCADE,
    )

    text = models.TextField(
        verbose_name='Текст ответа',
        null=True, blank=True,
    )

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = 'Ответ'
        verbose_name_plural = 'Ответы'


class UserQuestion(models.Model):

    user = models.ForeignKey(
        to='user.User',
        on_delete=models.CASCADE,
        verbose_name='Пользователь',
    )

    question = models.ForeignKey(
        to=Question,
        on_delete=models.CASCADE,
        verbose_name='Вопрос',
    )

    answer = models.TextField(
        verbose_name='Ответ на вопрос',
        null=True, blank=True,
    )

    variants = models.ManyToManyField(
        to=Answer,
        verbose_name='Варианты ответов',
        blank=True, null=True,
    )

    def __str__(self):
        return f'{self.question.text}: {self.answer}'

    class Meta:
        verbose_name = 'Ответы на вопрос'
        verbose_name_plural = 'Ответы на вопросы'


class Quiz(models.Model):

    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Дата создания',
    )

    questions = models.ManyToManyField(
        to=UserQuestion,
        verbose_name='Ответы на вопросы',
    )

    user = models.ForeignKey(
        to='user.User',
        on_delete=models.CASCADE,
        verbose_name='Пользователь'
    )

    def __str__(self):
        return f'{self.id or 0}'

    class Meta:
        verbose_name = 'Опрос'
        verbose_name_plural = 'Опросы'
