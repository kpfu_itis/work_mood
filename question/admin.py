from django.contrib.admin import ModelAdmin, register

from . import models


@register(models.Question)
class QuestionAdmin(ModelAdmin):
    pass


@register(models.Answer)
class AnswerAdmin(ModelAdmin):
    pass


@register(models.UserQuestion)
class UserQuestionAdmin(ModelAdmin):
    pass


@register(models.Quiz)
class QuizAdmin(ModelAdmin):
    pass
