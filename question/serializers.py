from rest_framework import serializers

from question.models import Quiz, UserQuestion


class UserQuestionsSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserQuestion
        fields = (
            'question',
            'answer',
            'variants',
        )


class QuizSerializer(serializers.ModelSerializer):

    questions = UserQuestionsSerializer(many=True)

    def create(self, validated_data):
        pass

    class Meta:
        model = Quiz
        fields = (
            'id',
            'questions',
            'created_at'
        )
