from django.contrib import admin
from django.urls import path
from rest_framework.routers import SimpleRouter

from . import views

urlpatterns = [
    path('', views.TrackerView.as_view(), name='tracker'),
]

quiz_router = SimpleRouter()
quiz_router.register('quiz', views.QuizView, basename='quiz')

urlpatterns += quiz_router.urls
