from django.contrib.admin import ModelAdmin, register

from pie import models


@register(models.Pie)
class PieAdmin(ModelAdmin):
    pass


@register(models.Sphere)
class SphereAdmin(ModelAdmin):
    pass


@register(models.Record)
class RecordAdmin(ModelAdmin):
    pass
