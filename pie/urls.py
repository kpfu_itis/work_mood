from django.contrib import admin
from django.urls import path
from rest_framework.routers import SimpleRouter

from . import views


urlpatterns = [
    path('pie-update/', views.pie_update, name='pie-update')
]

pie_router = SimpleRouter()
pie_router.register('', views.PieView, basename='pie')

urlpatterns += pie_router.urls



