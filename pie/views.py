from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin, ListModelMixin, UpdateModelMixin, \
    DestroyModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from pie.models import Pie, Record, Sphere
from pie.serializers import RecordSerializer
from question.serializers import QuizSerializer


class PieView(
    CreateModelMixin,
    RetrieveModelMixin,
    ListModelMixin,
    UpdateModelMixin,
    DestroyModelMixin,
    GenericViewSet
):

    queryset = Record.objects.all()
    serializer_class = RecordSerializer

    def get_queryset(self):
        queryset = super(PieView, self).get_queryset()
        return queryset.filter(user=self.request.user).order_by('-created_at')

    def list(self, request, *args, **kwargs):
        serializer = self.get_serializer(self.get_queryset(), many=True)
        return render(
            request,
            'pie/main.html',
            {
                'user': request.user,
                'records': serializer.data,
            }
        )

    def create(self, request, *args, **kwargs):
        record = Record()
        record.user = request.user
        record.title = request.data['record_title']
        record.save()
        for i in range(2):
            pie = Pie()
            pie.title = request.data[f'pie_title_{i + 1}']
            pie.record = record
            pie.save()
            titles = request.data[f'segment_title_{i + 1}'].split(',')
            points = request.data[f'segment_point_{i + 1}'].split(',')
            for title, point in zip(titles, points):
                segment = Sphere()
                segment.title = title
                segment.percent = point
                segment.pie = pie
                segment.save()

        return Response(200)


@login_required
def main(request):
    return render(
        request,
        'pie/main.html',
        {'user': request.user}
    )


def pie_update(request):
    record = Record.objects.filter(id=request.POST['id']).first()
    record.description = request.POST['description']
    record.save()
    return HttpResponse(200)