from django.db import models


class Record(models.Model):

    title = models.CharField(
        verbose_name='Название',
        max_length=100,
    )

    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Дата создания',
    )

    user = models.ForeignKey(
        to='user.User',
        on_delete=models.CASCADE,
        verbose_name='Пользователь'
    )

    description = models.TextField(
        verbose_name='Вывод',
        default='',
    )

    def __str__(self):
        return f'{self.title}'

    class Meta:
        verbose_name = 'Запись'
        verbose_name_plural = 'Запись'


class Pie(models.Model):

    title = models.CharField(
        verbose_name='Название',
        max_length=100,
    )

    record = models.ForeignKey(
        to=Record,
        verbose_name='Запись',
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f'{self.title}'

    class Meta:
        verbose_name = 'Пирог'
        verbose_name_plural = 'Пироги'


class Sphere(models.Model):

    title = models.CharField(
        verbose_name='Название',
        max_length=100,
    )

    percent = models.PositiveSmallIntegerField(
        verbose_name='Процетов'
    )

    pie = models.ForeignKey(
        to=Pie,
        verbose_name='Пирог',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return f'{self.title}: {self.percent}'

    class Meta:
        verbose_name = 'Сфера'
        verbose_name_plural = 'Сферы'
