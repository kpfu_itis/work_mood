# Generated by Django 3.2.4 on 2021-06-10 02:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pie', '0002_record_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='record',
            name='description',
            field=models.TextField(blank=True, null=True, verbose_name='Вывод'),
        ),
        migrations.AlterField(
            model_name='record',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Дата создания'),
        ),
    ]
