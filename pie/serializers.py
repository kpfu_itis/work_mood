from rest_framework import serializers

from pie.models import Record, Pie, Sphere


class SphereSerializer(serializers.ModelSerializer):

    class Meta:
        model = Sphere
        fields = (
            'title',
            'percent',
        )


class PieSerializer(serializers.ModelSerializer):

    spheres = SphereSerializer(
        source='sphere_set',
        many=True)

    class Meta:
        model = Pie
        fields = (
            'id',
            'title',
            'spheres',
        )


class RecordSerializer(serializers.ModelSerializer):

    pies = PieSerializer(
        source='pie_set',
        many=True)

    class Meta:
        model = Record
        fields = (
            'id',
            'title',
            'created_at',
            'pies',
            'description',
        )
