from django.contrib import admin
from django.urls import path

from user import views

urlpatterns = [
    path('login/', views.AuthView.as_view(), name='login'),
    path('logout/', views.logout, name='logout'),
    path('registration/', views.RegistrationView.as_view(), name='registration'),
    path('', views.main, name='main'),
]
