from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):

    REQUIRED_FIELDS = []

    groups = None
    first_name = None
    last_name = None

    fname = models.CharField(
        verbose_name='Фамилия',
        blank=True,
        null=True,
        max_length=150,
    )

    iname = models.CharField(
        verbose_name='Имя',
        blank=True,
        null=True,
        max_length=30,
    )

    oname = models.CharField(
        verbose_name='Отчество',
        blank=True,
        null=True,
        max_length=30,
    )

    @property
    def fio(self):
        return ' '.join(filter(None, (self.fname, self.iname, self.oname)))

    @property
    def short_fio(self):
        return (
            f'{self.fname}'
            f'{f" {self.iname[0]}."if self.iname else ""}'
            f'{f" {self.oname[0]}."if self.oname else ""}')

    class Meta:
        db_table = 'users'
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'
