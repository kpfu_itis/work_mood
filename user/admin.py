from django.contrib.admin import ModelAdmin, register

from . import models


@register(models.User)
class UserAdmin(ModelAdmin):
    pass
