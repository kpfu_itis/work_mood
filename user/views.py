from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.urls import reverse
from rest_framework.exceptions import ValidationError
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import IsAuthenticated

from . import serializers


class AuthView(GenericAPIView):
    # permission_classes = [IsAuthenticated]

    serializer_class = serializers.AuthSerializer

    def get(self, request, **kwargs):
        return render(request, 'user/login.html')

    def post(self, request, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        data = serializer.validated_data

        user = auth.authenticate(
            username=data['username'],
            password=data['password'])

        if user is not None:
            auth.login(request, user)
            return redirect(reverse('tracker'))

        else:
            return render(
                request, 'user/login.html', {'success': False})


def logout(request):
    auth.logout(request)
    return redirect(reverse('login'))


@login_required
def main(request):
    return redirect(reverse('tracker'))


class RegistrationView(GenericAPIView):
    serializer_class = serializers.RegistrationSerializer

    def get(self, request, **kwargs):
        return render(request, 'user/registration.html')

    def post(self, request, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = serializer.create(serializer.validated_data)

        user = auth.authenticate(
            username=user.username,
            password=serializer.validated_data['password'])

        if user is not None:
            auth.login(request, user)
            return redirect(reverse('tracker'))

        else:
            return render(
                request, 'user/registration.html', {'success': False})
