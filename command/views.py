from django.shortcuts import render

# Create your views here.


def main(request):
    return render(request, 'command/main.html', {'user': request.user})